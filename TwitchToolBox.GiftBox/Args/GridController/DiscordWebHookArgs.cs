﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TwitchToolBox.GiftBox.Args.GridController
{
    public class DiscordWebHookArgs
    {
        public String WebHookUrl { get; set; }
        public String MessageTemplate { get; set; }
    }
}
