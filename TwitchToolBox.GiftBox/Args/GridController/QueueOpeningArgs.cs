﻿namespace TwitchToolBox.GiftBox.Args.GridController
{
    public class QueueOpeningArgs
    {
        public int CaseId { get; set; }
        public string ReservedId { get; set; }
    }
}