﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TwitchToolBox.GiftBox.Engine;

namespace TwitchToolBox.GiftBox.Controllers
{
    [Route("{gridCode}/[controller]/[action]")]
    [ApiExplorerSettings(IgnoreApi = true)]
    public class BitBoxController : Controller
    {
        public GridEngine _engine { get; set; }
        public BitBoxController(GridEngine engine)
        {
            _engine = engine;
        }

        public IActionResult Grid(String gridCode)
        {
            ViewBag.gridCode = gridCode;
            return View();
        }

        public IActionResult Moderator(String gridCode, [FromQuery]String password)
        {
            ViewBag.gridCode = gridCode;
            if (_engine[gridCode].Password != password)
                return new ContentResult
                {
                    StatusCode = 403
                };
            return View();
        }

        public IActionResult OpenGift(String gridCode, [FromQuery]String password)
        {
            ViewBag.gridCode = gridCode;
            if (_engine[gridCode].Password != password)
                return new ContentResult
                {
                    StatusCode = 403
                };
            return View();
        }

        public IActionResult LastOpened(String gridCode)
        {
            ViewBag.gridCode = gridCode;
            return View();
        }
    }
}