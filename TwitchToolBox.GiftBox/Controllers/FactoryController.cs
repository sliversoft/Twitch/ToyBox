﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TwitchToolBox.GiftBox.Engine;

namespace TwitchToolBox.GiftBox.Controllers
{
    [Produces("application/json")]
    public class FactoryController : Controller
    {
        public GridEngine _engine { get; set; }
        public FactoryController(GridEngine engine)
        {
            _engine = engine;
        }

        [HttpGet]
        [Route("api/Grid/[action]")]
        [ProducesResponseType(typeof(String), 200)]
        public IActionResult Generate([FromQuery]int rows, [FromQuery]int cols)
        {
            return Ok(_engine.CreateNew(rows, cols).GridCode);
        }
    }
}