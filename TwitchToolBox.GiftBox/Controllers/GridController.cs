﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using TwitchToolBox.GiftBox.Args.GridController;
using TwitchToolBox.GiftBox.Engine;

namespace TwitchToolBox.GiftBox.Controllers
{
    [Produces("application/json")]
    [Route("{gridCode}/api/[controller]/[action]")]
    public class GridController : Controller
    {
        public GridEngine _engine { get; set; }
        public GridController(GridEngine engine)
        {
            _engine = engine;
        }

        [HttpGet]
        public IActionResult Get(String gridCode)
        {
            return Ok(_engine[gridCode].Clone());
        }

        [HttpGet("{id}")]
        public IActionResult Open(String gridCode, int id)
        {
            Model.Grid grid = _engine[gridCode];
            return base.Ok(grid.Open(id - grid.GridStartNumerotation));
        }

        [HttpGet]
        public IActionResult NextOpening(String gridCode)
        {
            var reserved = _engine[gridCode].NextOpening;
            if (reserved == null)
                return NotFound();
            return Ok(reserved);
        }

        [HttpGet]
        public IActionResult NextOpenings(String gridCode)
        {
            Model.Grid grid = _engine[gridCode];
            List<Model.ReservedCase> reserved = grid.NextOnes;
            if (reserved == null || reserved.Count() <= 0)
                return NotFound();
            reserved = reserved.Select(s => (Model.ReservedCase)s.Clone()).ToList();
            reserved.ForEach(fe => fe.ReservedId += grid.GridStartNumerotation);
            return Ok(reserved);
        }

        [HttpPost]
        public IActionResult QueueOpening(String gridCode, [FromBody]QueueOpeningArgs queuedOpening)
        {
            try
            {
                Model.Grid grid = _engine[gridCode];
                List<Model.ReservedCase> queue = grid.Reserve(queuedOpening.CaseId - grid.GridStartNumerotation, queuedOpening.ReservedId).Select(s => (Model.ReservedCase)s.Clone()).ToList();
                queue.ForEach(fe => fe.ReservedId += grid.GridStartNumerotation);
                return base.Ok(queue);
            }
            catch (ApplicationException ae)
            {
                return new ContentResult
                {
                    StatusCode = 409,
                    Content = ae.Message
                };
            }
            catch (NullReferenceException nre)
            {
                return new ContentResult
                {
                    StatusCode = 500,
                    Content = nre.Message
                };
            }
        }

        [HttpPost]
        public IActionResult DiscordWebHook(String gridCode, [FromBody]DiscordWebHookArgs Payload)
        {
            _engine[gridCode].DiscordMessageTemplate = Payload.MessageTemplate;
            _engine[gridCode].DiscordWebHookUrl = Payload.WebHookUrl;

            return Ok();
        }

        [HttpGet]
        public IActionResult RemainingGifts(String gridCode)
        {
            return Ok(_engine[gridCode].Cases.GroupBy(gb => gb.Content).Select(s => new { Gift = s.First().Content, Count = s.Count() }).Where(w => w.Gift != null));
        }

        [HttpPost]
        public IActionResult InjectGifts(String gridCode, [FromBody]String injectedGift)
        {
            var injectedGifts = injectedGift.Replace(Environment.NewLine, "¤").Replace(";", "¤").Split("¤");
            Regex reg = new Regex(@"^\[([0-9]+)-([0-9]+)\]\(([0-9]+)\)([^#\r\n]*)(#[0-9A-Fa-f]{6,6})?$");

            Model.Grid grid = _engine[gridCode];
            grid.Cases.ForEach(fe =>
            {
                fe.Content = null;
                fe.IsOpened = false;
                fe.Color = "#000000";
            });
            _engine[gridCode].LastOpened = (null, null, 0, null);

            foreach (var regGift in injectedGifts)
            {
                var match = reg.Match(regGift);
                int minRange = int.Parse(match.Groups[1].Value) - grid.GridStartNumerotation;
                int maxRange = int.Parse(match.Groups[2].Value) - grid.GridStartNumerotation;
                int count = int.Parse(match.Groups[3].Value);
                String content = match.Groups[4].Value;
                String color = match.Groups[5].Success ? match.Groups[5].Value : _engine.GenerateRandomHexColor();
                for (int i = 0; i < count; i++)
                {
                    grid.InjectGift(minRange, maxRange, content, color);
                }
            }
            return Ok(gridCode);
        }

        [HttpGet]
        public IActionResult Randomize(String gridCode)
        {
            Model.Grid grid = _engine[gridCode];
            grid.Randomize();
            return Ok();
        }

        [HttpGet]
        public IActionResult SetState(String gridCode, Boolean state)
        {
            _engine[gridCode].Cases.ForEach(fe => fe.IsOpened = state);
            _engine[gridCode].LastOpened = (null, null, 0, null);
            return Ok();
        }

        [HttpDelete("{caseId}")]
        public IActionResult SpecificReservedCase(string gridCode, int caseId)
        {
            try
            {
                _engine[gridCode].RemoveSpecifiedReserved(caseId - _engine[gridCode].GridStartNumerotation);
                return Ok();
            }
            catch (Exception)
            {
                return new ContentResult
                {
                    StatusCode = 500
                };
            }
        }

        [HttpGet]
        public IActionResult LastOpened(String gridCode)
        {
            if (String.IsNullOrWhiteSpace(_engine[gridCode].LastOpened.Viewer))
                return NotFound();
            var (Viewer, Gift, CaseId, Color) = _engine[gridCode].LastOpened;
            return Ok(new { CaseId = CaseId + _engine[gridCode].GridStartNumerotation,  Viewer,  Gift, Color });
        }
    }
}