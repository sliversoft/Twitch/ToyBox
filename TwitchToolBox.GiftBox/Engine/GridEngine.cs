﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TwitchToolBox.GiftBox.Model;

namespace TwitchToolBox.GiftBox.Engine
{
    public class GridEngine
    {
        public GridEngine()
        {
            if (File.Exists("Engine.json"))
            {
                _grids = JsonConvert.DeserializeObject<Dictionary<String, Grid>>(File.ReadAllText("Engine.json"));
                foreach (var item in _grids.Select(s => s.Value))
                {
                    item.OnCaseOpened += Grid_OnCaseOpened;
                }
            }
            else
            {
                var grid = CreateNew(10, 20, "maho_tv");
                grid.DiscordWebHookUrl = "https://discordapp.com/api/webhooks/424026612825194497/sGymxqEWXrLVDxNbSKuoC8kBLh0HFEVMSpoCZzqfYyNb6BC7sJ0yisiYyppiwP5xwBXz";
                grid.DiscordMessageTemplate = "[Prototype] Hey @here faites un maximum de bruit, @user viens d'ouvrir la case @case de la BitBox de @gridCode, elle contenait: @content";
                grid.GridStartNumerotation = 100;
                grid.Password = "test";
                grid.OnCaseOpened += Grid_OnCaseOpened;
                //for (int i = 0; i < 850; i++) { grid.InjectGift(0, 1899, GenerateCode(4), GenerateRandomHexColor()); };
                //grid.InjectGift(0, 1899, "Cadeau",GenerateRandomHexColor());
                //grid.InjectGift(0, 1899, "Cadeau", GenerateRandomHexColor());
                //grid.Cases.ForEach(fe => fe.IsOpened = true);
            }
        }

        private void Grid_OnCaseOpened(object sender, EventArgs e)
        {
            String str = JsonConvert.SerializeObject(_grids, Formatting.Indented);
            File.WriteAllText("Engine.json", str);
        }

        private Dictionary<String, Grid> _grids = new Dictionary<String, Grid>();
        private const String _characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        private const String _colorCharacters = "0123456789ABCDEF";

        public Grid this[string code]
        {
            get
            {
                if (!_grids.ContainsKey(code))
                    throw new IndexOutOfRangeException();
                return _grids[code];
            }
        }

        public Grid CreateNew(int rows, int cols)
        {
            return CreateNew(rows, cols, GenerateCode(10));
        }

        public Grid CreateNew(int rows, int cols, String code)
        {
            Grid result = new Grid { Rows = rows, Cols = cols, GridCode = code };

            for (int i = 0; i < rows * cols; i++)
            {
                result.Cases.Add(new Case());
            }
            _grids.Add(result.GridCode, result);
            result.OnCaseOpened += Grid_OnCaseOpened;

            return result;
        }

        private String GenerateCode(int maxLength)
        {
            Random random = new Random();
            StringBuilder strBuilder = new StringBuilder();
            for (int i = 0; i < maxLength; i++)
            {
                strBuilder.Append(_characters[random.Next(0, _characters.Length)]);
            }

            return strBuilder.ToString();
        }
        public String GenerateRandomHexColor()
        {
            Random random = new Random();
            StringBuilder strBuilder = new StringBuilder("#");
            for (int i = 0; i < 6; i++)
            {
                strBuilder.Append(_colorCharacters[random.Next(0, _colorCharacters.Length)]);
            }
            return strBuilder.ToString();
        }
    }
}
