﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TwitchToolBox.GiftBox.Model
{
    public class Case
    {
        public String Content { get; set; }
        public Boolean IsOpened { get; set; }
        public String Color { get; set; } = "#000000";
    }
}
