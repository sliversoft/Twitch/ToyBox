﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TwitchToolBox.GiftBox.Model
{
    public class Grid : ICloneable
    {
        #region Properties
        public String GridCode { get; set; }
        public String DiscordWebHookUrl { get; set; }
        public String DiscordMessageTemplate { get; set; }
        public int GridStartNumerotation { get; set; }
        public List<Case> Cases { get; set; } = new List<Case>();
        public int Rows { get; set; }
        public int Cols { get; set; }
        public String Password { get; set; }
        public (String Viewer, String Gift, int CaseId, String Color) LastOpened { get; set; }


        [JsonIgnore]
        public ReservedCase NextOpening
        {
            get
            {
                if (!QueuedRoll.TryPeek(out ReservedCase @case))
                {
                    return null;
                }

                @case = (ReservedCase)QueuedRoll.Peek().Clone();
                @case.ReservedId += GridStartNumerotation;
                return @case;
            }
        }
        [JsonIgnore]
        public List<ReservedCase> NextOnes
        {
            get
            {
                if (!QueuedRoll.TryPeek(out ReservedCase @case))
                {
                    return null;
                }

                return QueuedRoll.ToList();
            }
        }

        private Queue<ReservedCase> QueuedRoll { get; set; } = new Queue<ReservedCase>();
        #endregion

        public Case this[int caseId]
        {
            get { return Cases[caseId]; }
        }

        public Case Open(int caseId)
        {
            if (QueuedRoll.TryPeek(out ReservedCase reserved) && Cases[reserved.ReservedId] != Cases[caseId])
                throw new ApplicationException();
            Cases[caseId].IsOpened = true;

            #region WebHook
            try
            {
                if (!String.IsNullOrWhiteSpace(DiscordWebHookUrl))
                {
                    var request = System.Net.WebRequest.CreateHttp(DiscordWebHookUrl);

                    var payload = new
                    {
                        content = DiscordMessageTemplate
                        .Replace("@case", (caseId + GridStartNumerotation).ToString())
                        .Replace("@gridCode", GridCode.ToString())
                        .Replace("@content", (!String.IsNullOrWhiteSpace(Cases[caseId].Content)) ? Cases[caseId].Content : "Rien")
                        .Replace("@user", (reserved != null) ? reserved.ReservedFor : "Quelqu'un")
                    };
                    var data = Encoding.ASCII.GetBytes(JsonConvert.SerializeObject(payload));

                    request.Method = "POST";
                    request.ContentType = "application/json";
                    request.ContentLength = data.Length;

                    var bodyStream = request.GetRequestStream();
                    bodyStream.Write(data, 0, data.Length);
                    bodyStream.Close();

                    request.GetResponse();
                }
                LastOpened = (
                (reserved != null) ? reserved.ReservedFor : "Quelqu'un",
                (!String.IsNullOrWhiteSpace(Cases[caseId].Content)) ? Cases[caseId].Content : "Rien",
                caseId,
                Cases[caseId].Color);
                OnCaseOpened?.Invoke(this, new EventArgs() { });
            }
            catch (Exception)
            { }

            #endregion
            
            return QueuedRoll.TryDequeue(out ReservedCase outCase) ? Cases[outCase.ReservedId] : Cases[caseId];
        }
        public List<ReservedCase> Reserve(int caseId, String reservedBy)
        {
            ReservedCase rcComparer = new ReservedCase { ReservedId = caseId, ReservedFor = reservedBy };
            if (QueuedRoll.ToList().Any(a => a.ReservedId == caseId))
            {
                throw new ApplicationException($"These case is already reserve by { reservedBy }");
            }

            if (Cases[caseId].IsOpened)
                throw new ApplicationException("These case is already open");
            QueuedRoll.Enqueue(rcComparer);
            return QueuedRoll.ToList();
        }

        public void RemoveSpecifiedReserved(int caseId)
        {
            QueuedRoll = new Queue<ReservedCase>(QueuedRoll.Where(w => w.ReservedId != caseId));
        }

        public void InjectGift(int minRange, int maxRange, String content, String color)
        {
            Random random = new Random();
            var caseId = random.Next(minRange, maxRange + 1);

            var isEligible = Cases.GetRange(minRange, (maxRange - minRange + 1)).Count(c => c.Content == null) >= 1;
            if (!isEligible)
                throw new ArgumentOutOfRangeException();

            while (Cases[caseId].Content != null)
                caseId = random.Next(minRange, maxRange + 1);

            Cases[caseId].Content = content;
            Cases[caseId].Color = color;
            OnCaseOpened?.Invoke(this, new EventArgs() { });
        }

        public void Randomize()
        {
            Cases = Cases.OrderBy(ob => Guid.NewGuid()).ToList();
            OnCaseOpened?.Invoke(this, new EventArgs() { });
        }

        public object Clone()
        {
            Grid clone = new Grid
            {
                GridCode = this.GridCode,
                DiscordWebHookUrl = this.DiscordWebHookUrl,
                DiscordMessageTemplate = this.DiscordMessageTemplate,
                GridStartNumerotation = this.GridStartNumerotation,
                Rows = this.Rows,
                Cols = this.Cols
            };
            this.Cases.ForEach(fe => clone.Cases.Add(new Case { IsOpened = fe.IsOpened, Content = (fe.IsOpened) ? fe.Content : null, Color = fe.Color }));

            return clone;
        }

        public delegate void CaseOpened(object sender, EventArgs e);
        public event CaseOpened OnCaseOpened;
    }
}
