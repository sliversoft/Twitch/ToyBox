﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TwitchToolBox.GiftBox.Model
{
    public class ReservedCase : ICloneable
    {
        public int ReservedId { get; set; }
        public String ReservedFor { get; set; }

        public object Clone()
        {
            return new ReservedCase {
                ReservedFor = this.ReservedFor,
                ReservedId = this.ReservedId
            };
        }

        public new bool Equals(object x, object y)
        {
            return ((ReservedCase)x).ReservedId == ((ReservedCase)y).ReservedId;
        }
    }
}
