﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Swashbuckle.AspNetCore.Swagger;
using TwitchToolBox.GiftBox.Engine;

namespace TwitchToolBox.GiftBox
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton<GridEngine>();

            services.AddSwaggerGen(options =>
            {
                options.IgnoreObsoleteActions();
                options.OrderActionsBy(description => description.HttpMethod);
                options.SwaggerDoc("GiftBox", new Info
                {
                    Title = "GiftBox",
                    Version = "beta",
                    Contact = new Contact
                    {
                        Email = "remi@swiderski.me",
                        Name = "Rémi Swiderski",
                        Url = "https://github.com/rswiderski"
                    },
                    License = new License
                    {
                        Name = "MIT"
                    }
                });
            });
            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseStaticFiles();

            app.UseSwagger();
            app.UseSwaggerUI(setup =>
            {
                setup.DocExpansion(DocExpansion.List);
                setup.DefaultModelRendering(ModelRendering.Example);
                setup.SwaggerEndpoint("/swagger/GiftBox/swagger.json", "GiftBox");
            });
            app.UseMvc();
        }
    }
}
