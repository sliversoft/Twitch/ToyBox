﻿var app = new Vue({
    el: '#app',
    data: {
        user: null,
        caseId: null,
        queue: null
    },
    methods: {
        getNext: function () {
            $.ajax({
                context: this,
                url: '/' + gridCode + '/api/Grid/NextOpenings',
                type: "GET",
                success: function (result) {
                    app.queue = result;
                },
                error: function (jqXHR, exception) {
                    if (jqXHR.status === 404) {
                        app.queue = null;
                    }
                }
            });
        },
        reserve: function () {
            var queueArgs = {
                "CaseId": this.caseId,
                "ReservedId": this.user
            };
            $.ajax({
                url: '/' + gridCode + '/api/Grid/QueueOpening',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                async: true,
                cache: false,
                type: 'POST',
                data: JSON.stringify(queueArgs),
                dataType: "json",
                success: function (result) {
                    
                },
                error: function (jqXHR, exception) {
                    if (jqXHR.status === 409) {
                        alert(jqXHR.responseText);
                    } else if (jqXHR.status === 500) {
                        alert("Une erreur est survenue.");
                    }
                    console.log(jqXHR);
                }
            });
            this.caseId = null;
            this.user = null;
        },
        open: function (id) {
            $.ajax({
                type: "GET",
                url: '/' + gridCode + '/api/Grid/Open/' + id,
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                async: true,
                cache: false,
                success: function (result) {

                },
                error: function (jqXHR, exception) {
                    console.log(exception);
                    console.log(jqXHR);
                }
            });
        },
        remove: function (id) {
            $.ajax({
                type: "DELETE",
                url: '/' + gridCode + '/api/Grid/SpecificReservedCase/' + id,
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                async: true,
                cache: false,
                success: function (result) {

                },
                error: function (jqXHR, exception) {
                    
                }
            });
        }
    }
});
setInterval(app.getNext, 500);