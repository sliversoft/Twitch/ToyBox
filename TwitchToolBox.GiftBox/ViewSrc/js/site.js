﻿var app = new Vue({
    el: '#app',
    data: {
        grid: null,
        message: null,
        nextOpening: null
    },
    methods: {
        getGrid: function () {
            $.ajax({
                context: this,
                url: '/' + gridCode + '/api/Grid/Get',
                type: "GET",
                success: function (result) {
                    app.message = result;
                    app.grid = createGroupedArray(result.cases, result.cols);
                }
            });
        },
        open: function (id) {
            $.ajax({
                context: this,
                url: '/' + gridCode + '/api/Grid/Open/' + id,
                type: "GET",
                success: function (result) {
                    app.getGrid();
                    app.nextRoll();
                }
            });
        },
        nextRoll: function () {
            $.ajax({
                context: this,
                url: '/' + gridCode + '/api/Grid/NextOpening/',
                type: "GET",
                success: function (result) {
                    app.nextOpening = result;
                },
                error: function (request, status, error) {
                    if (request.status === 404) {
                        app.nextOpening = null;
                    }
                }
            });
        }
    }
});
var createGroupedArray = function (arr, chunkSize) {
    var groups = [], i;
    for (i = 0; i < arr.length; i += chunkSize) {
        groups.push(arr.slice(i, i + chunkSize));
    }
    return groups;
};
function refresh() {
    app.getGrid();
}
refresh();
setInterval(refresh, 500);